﻿namespace Fortigate.Wrapper
{
    public class FortigateOptions
    {
        public string Host { get; set; }
        public string Token { get; set; }
    }
}