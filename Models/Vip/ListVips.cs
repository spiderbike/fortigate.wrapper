﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fortigate.Wrapper.Models.Vip
{
    public class ListVips
    {
        public string http_method { get; set; }
        public string revision { get; set; }
        public ListVipsResult[] results { get; set; }
        public string vdom { get; set; }
        public string path { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public int http_status { get; set; }
        public string serial { get; set; }
        public string version { get; set; }
        public int build { get; set; }
    }

    public class ListVipsResult
    {
        public string name { get; set; }
        public string q_origin_key { get; set; }
        public int id { get; set; }
        public string uuid { get; set; }
        public string comment { get; set; }
        public string type { get; set; }
        public int dnsmappingttl { get; set; }
        public string ldbmethod { get; set; }
        public string[] srcfilter { get; set; }
        public string[] service { get; set; }
        public string extip { get; set; }
        public string[] extaddr { get; set; }
        public ListVipsMappedip[] mappedip { get; set; }
        public string mappedaddr { get; set; }
        public string extintf { get; set; }
        public string arpreply { get; set; }
        public string servertype { get; set; }
        public string httpredirect { get; set; }
        public string persistence { get; set; }
        public string natsourcevip { get; set; }
        public string portforward { get; set; }
        public string protocol { get; set; }
        public string extport { get; set; }
        public string mappedport { get; set; }
        public int gratuitousarpinterval { get; set; }
        public string[] srcintffilter { get; set; }
        public string portmappingtype { get; set; }
        public string[] realservers { get; set; }
        public string httpcookiedomainfromhost { get; set; }
        public string httpcookiedomain { get; set; }
        public string httpcookiepath { get; set; }
        public int httpcookiegeneration { get; set; }
        public int httpcookieage { get; set; }
        public string httpcookieshare { get; set; }
        public string httpscookiesecure { get; set; }
        public string httpmultiplex { get; set; }
        public string httpipheader { get; set; }
        public string httpipheadername { get; set; }
        public string outlookwebaccess { get; set; }
        public string weblogicserver { get; set; }
        public string websphereserver { get; set; }
        public string sslmode { get; set; }
        public string sslcertificate { get; set; }
        public string ssldhbits { get; set; }
        public string sslalgorithm { get; set; }
        public string[] sslciphersuites { get; set; }
        public string sslserveralgorithm { get; set; }
        public string[] sslserverciphersuites { get; set; }
        public string sslpfs { get; set; }
        public string sslminversion { get; set; }
        public string sslmaxversion { get; set; }
        public string sslserverminversion { get; set; }
        public string sslservermaxversion { get; set; }
        public string sslsendemptyfrags { get; set; }
        public string sslclientfallback { get; set; }
        public string sslclientrenegotiation { get; set; }
        public string sslclientsessionstatetype { get; set; }
        public int sslclientsessionstatetimeout { get; set; }
        public int sslclientsessionstatemax { get; set; }
        public int sslclientrekeycount { get; set; }
        public string sslserversessionstatetype { get; set; }
        public int sslserversessionstatetimeout { get; set; }
        public int sslserversessionstatemax { get; set; }
        public string sslhttplocationconversion { get; set; }
        public string sslhttpmatchhost { get; set; }
        public string sslhpkp { get; set; }
        public string sslhpkpprimary { get; set; }
        public string sslhpkpbackup { get; set; }
        public int sslhpkpage { get; set; }
        public string sslhpkpreporturi { get; set; }
        public string sslhpkpincludesubdomains { get; set; }
        public string sslhsts { get; set; }
        public int sslhstsage { get; set; }
        public string sslhstsincludesubdomains { get; set; }
        public string[] monitor { get; set; }
        public int maxembryonicconnections { get; set; }
        public int color { get; set; }
    }

    public class ListVipsMappedip
    {
        public string range { get; set; }
        public string q_origin_key { get; set; }
    }

}
