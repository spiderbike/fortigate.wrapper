﻿using System;

namespace Fortigate.Wrapper
{
    public static class Endpoints
    {
       
        public static string Firewall(string ip)
        {
            return $"https://{ip}/api/v2/";
        }
    }

}