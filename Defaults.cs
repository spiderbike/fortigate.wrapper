﻿using System;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Fortigate.Wrapper
{
    public class Defaults
    {
        class MyFormatter : JsonMediaTypeFormatter
        {
            public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
            {
                if (type == null) throw new ArgumentNullException(nameof(type));
                if (headers == null) throw new ArgumentNullException(nameof(headers));

                if (mediaType != null)
                {
                    headers.ContentType = (MediaTypeHeaderValue)((ICloneable)mediaType).Clone();
                }

                // If content type is not set then set it based on supported media types.
                if (headers.ContentType == null)
                {
                    MediaTypeHeaderValue defaultMediaType = null;
                    if (SupportedMediaTypes.Count > 0)
                    {
                        defaultMediaType = SupportedMediaTypes[0];
                    }
                    if (defaultMediaType != null)
                    {
                        headers.ContentType = (MediaTypeHeaderValue)((ICloneable)DefaultMediaType).Clone();
                    }
                }
            }
        }

        public static JsonMediaTypeFormatter DefaultFormatterNoCharset = new MyFormatter
        {
            SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            }
        };

        public static JsonMediaTypeFormatter DefaultFormatterNoCharsetIncludeNullValues = new MyFormatter
        {
            SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                MissingMemberHandling = MissingMemberHandling.Ignore
            }
        };

        public static JsonMediaTypeFormatter DefaultFormatter = new JsonMediaTypeFormatter
        {
            SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            }
        };


        public static JsonMediaTypeFormatter DefaultFormatterIncludeNullValues = new JsonMediaTypeFormatter
        {
            SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                MissingMemberHandling = MissingMemberHandling.Ignore
            }
        };

        public static JsonSerializerSettings DefaultJsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static JsonSerializer DefaultJsonSerializer()
        {
            var serializer = new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented,
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };
            return serializer;
        }

    }
}