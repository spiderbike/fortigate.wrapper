﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Polly;

namespace Fortigate.Wrapper
{
    public class FortigateApi
    {
        private Polly.Retry.AsyncRetryPolicy _defaultRetryPolicy;
        private readonly HttpClient _httpClient;

        public FortigateApi(HttpClient httpClient)
        {
            _defaultRetryPolicy = Policy
                                   .Handle<HttpRequestException>()
                                   .RetryAsync(3, onRetry: async (exception, retryCount) =>
                                   {
                                       //await Console.Out.WriteLineAsync($"	\\_(-_-)_/");
                                       //await Console.Out.WriteLineAsync($"Well that didn't work");
                                       //await Console.Out.WriteLineAsync(exception.Message);
                                       //await Console.Out.WriteLineAsync(exception.StackTrace);
                                       //await Console.Out.WriteLineAsync($"we are going to try again. cross your fingers.");
                                   });
            _httpClient = httpClient;
        }

        public async Task<T> DoHttpRequest<T>(Func<HttpClient, Task<HttpResponseMessage>> httpCall)
        {
            HttpResponseMessage response = await _defaultRetryPolicy.ExecuteAsync(() => MakeRequest(httpCall, _httpClient));
            return await response.Content.ReadAsAsync<T>();
        }



        public static async Task<HttpResponseMessage> MakeRequest(Func<HttpClient, Task<HttpResponseMessage>> httpCall, HttpClient client)
        {
            var response = await httpCall(client);

            if (!response.IsSuccessStatusCode)
            {
                var responseResult = await response.Content.ReadAsStringAsync();

                await Console.Out.WriteLineAsync(JsonConvert.SerializeObject(responseResult, Defaults.DefaultFormatter.SerializerSettings));

                response.EnsureSuccessStatusCode();
            }

            return response;
        }

    }
}
