﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Fortigate.Wrapper
{
    public class Vips
    {
        public FortigateOptions _fortigateOptions;
        private readonly FortigateApi _apiClient;

        public Vips(FortigateApi apiClient, IOptions<FortigateOptions> fortigateOptions)
        {
            _apiClient = apiClient;
            _fortigateOptions = fortigateOptions.Value;
        }

        public async Task<Models.Vip.ListVips> ListAllVips()
        {
            return await _apiClient.DoHttpRequest<Models.Vip.ListVips>(c => c.GetAsync($"{Endpoints.Firewall(_fortigateOptions.Host)}/cmdb/firewall/vip?vdom=root&access_token={_fortigateOptions.Token}"));
        }
    }
}
